## M11-SAD 
## M11-SAD Seguretat i Alta Disponibilitat 

https://sites.google.com/site/asixm11edt/ 

### ASIX-M11-UF1 - Sseguretat física, lògica i legislació (24h)

#### NF1 Seguretat en sistemes de fitxers (8h)
01 - Permisos especials i ACLs
02 - RAID
03 - LVM
04 - Backups
#### NF2 Criptografia (8h) 
#### NF3 Certificats digitals (8h) 
05 - Certificats Digitals / GPG
06 - FileSystem Crypt
#### NFx Legislació 
07 - Legislació

#### ASIX-M11-UF2 - Seguretat activa i accés remot (24h) 

#### NF1 Mecanismes de seguretat activa (10h) 
01 - TCP-Wrappers
02 - Kerberos
#### NF2 Tècniques segures d'accés remot (14h) 
03 - Tunnel SSH
04 - VPN / IPSEC
05 - Certificas Digitals /OpenSSL

### ASIX-M11-UF3 - Tallafocs i servidors intermediàris (27h) 

#### NF1 Tallafocs (20h) 
01 - Firewall
#### NF2 Proxy (7h) 
02 - Proxy

### ASIX-M11-UF4 - Alta disponibilitat (24h)  

#### NF1 Alta disponibilitat (12h) 
01 - Alta Disponibilitat (clustering)
#### NF2 Virtualització (12h)
02 - Virtualització
