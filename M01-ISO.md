## M01-ISO
## M01-ISO Implantació de Sistemes Operatius

https://sites.google.com/site/asixm01edt 

### ASIX-M01-UF1 - Instal·lació, configuració i explotació d'un sistema informàtic (60h)

#### NF1 Instal·la el sistema operatiu.
    01 - Ordres de propòsit general.
    02 - Ordres de fitxers.
    03 - Permisos bàsics i avançats.
#### NF2 Administra el sistema operatiu.
    04 - Filtres: filtres, redireccionaments, pipes (bash).
    05 - Processos.

#### ASIX-M01-UF2 - Gestió de la informació i recursos de xarxa (80h)
##### NF3 Assegura i centralitza la informació.
    06 - PER: tractament de text, patrons d'expressions regulars.
    07 - Scripting bàsic.
    08 - Shell: bash al complert.
##### NF4 Administració avançada.
    09 - Administació d'usuaris.
    10 - Administració de disc.
    11 - Logs i monitorització.
    12 - Backups.
    13 - Arrancada del sistema.

#### ASIX-M01-UF3 - Implantació de programari específic (25h)
##### NF5 Programari específic.
    14 - Serveis de xarxa.
    15 - Administració remota.
    16 - Gestió de paquets i repositoris.
    17 - Repositoris pròpis i construcció de rpms.

#### ASIX-M01-UF4 - Seguretat, rendiment i recursos (33h)
##### NF6 Seguretat, rendiment i recursos.
    18 - Logs i monitorització de recursos.
    19 - Sistemes Duals.

