## M06-ASO 
## M06-ASO Administració de sistemes Operatius

https://sites.google.com/site/asixm06edt/ 

### ASIX-M06-UF1 - Administració avançada de sistemes operatius (90h) 

#### NF1 Servei de directori: LDAP (40h)
01 - LDAP   
02 - PAM 

#### NF2 Serveis d'accés i admin remota (12h)
03 - SSH, rsync, vnc, vino-server, spice 

#### NF3 Servei d'impressió: CUPS (12h)   
04 - CUPS 

#### NF4 Integració de xarxes heterogènies: SAMBA (14h)
05 -SAMBA 

#### NF5 Processos del sistema (12h)
06 -Processos

### ASIX-M06-UF2 - Automatització de tasques i llenguatge de guions  (42h)

### NF1 Escriptura d'scripts (20h)
01 - Scripts Python
  * named pipes
  * signals
  * sockets
  * fork/execv

#### NF2 Automatització de tasques i llenguatge de guions (22h)
02 - Client/Servidor